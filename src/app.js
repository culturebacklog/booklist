import React, { Component } from 'react';
import { View } from 'react-native';
import firebase from 'firebase';
import { Header, Button, Spinner } from './components/common';
import LoginForm from './components/LoginForm';
import BookList from './components/BookList';

class App extends Component {
  state = { loggedIn: null };

  componentWillMount() {
    firebase.initializeApp({
      apiKey: 'AIzaSyCaIKArpNSTM3uOIOEmuKuROF0bw7DNI7E',
      authDomain: 'booklist-70db6.firebaseapp.com',
      databaseURL: 'https://booklist-70db6.firebaseio.com',
      projectId: 'booklist-70db6',
      storageBucket: 'booklist-70db6.appspot.com',
      messagingSenderId: '234041316090'
    });

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({ loggedIn: true });
      } else {
        this.setState({ loggedIn: false });
      }
    });
  }

  renderContent() {
    switch (this.state.loggedIn) {
      case true:
        return (
          <BookList />
        );
      case false:
        return <LoginForm />;
      default:
        return <Spinner size="large" />;
    }
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header headerText="Books" />
        {this.renderContent()}
      </View>
    );
  }
}

export default App;
