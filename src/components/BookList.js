import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import axios from 'axios';
import BookDetail from './BookDetail';
import firebase from 'firebase';
import { View, Button } from './common';

class BookList extends Component {
  state = { books: [] };

  componentWillMount() {
    axios.get('https://www.googleapis.com/books/v1/volumes?q=harrypotter')
      .then(response => this.setState({ books: response.data.items }));
  }

  renderBooks() {
    return this.state.books.map(item =>
      <BookDetail key={item.id} book={item} />
    );
  }

  render() {
    console.log(this.state.books);

    return (
      <ScrollView>
        <Button onPress={() => firebase.auth().signOut()}>
          Log Out
          </Button>
        {this.renderBooks()}
      </ScrollView>
    );
  }
}

export default BookList;
